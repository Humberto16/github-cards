# GitSearch

Exercício simples de React para praticar Componentização, Props, UseState e também integração com a API do GitHub. 

Link do [Github](https://github.com/HumbertoSilv/GithubSearch) 

Link do [GitSearch](https://github-cards-dun.vercel.app/)

![screen1.png](screen01.png)
![screen2.png](screen02.png)
![screen3.png](screen03.png)
